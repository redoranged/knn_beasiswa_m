<?php
// To Get Current Path
// die(getcwd());

require_once 'init.php';

// Controller Setup
if(isset($_POST['model']))
{
    require_once load_controller($_POST['model']);
}

if(isset($_GET['model']))
{
    require_once load_controller($_GET['model']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=TITLE_NAME?></title>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!--Import materialize.min.css-->
    <link type="text/css" rel="stylesheet" href="<?=asset('materialize/css/materialize.min.css')?>"  media="screen,projection"/>

    <style>
        /* Select2 */
        .select2 .selection .select2-selection--single, .select2-container--default .select2-search--dropdown .select2-search__field {
            border-width: 0 0 1px 0 !important;
            border-radius: 0 !important;
            height: 2.05rem;
        }

        .select2-container--default .select2-selection--multiple, .select2-container--default.select2-container--focus .select2-selection--multiple {
            border-width: 0 0 1px 0 !important;
            border-radius: 0 !important;
        }

        .select2-results__option {
            color: #26a69a;
            padding: 8px 16px;
            font-size: 16px;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #eee !important;
            color: #26a69a !important;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #e1e1e1 !important;
        }

        .select2-dropdown {
            border: none !important;
            box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
        }

        .select2-container--default .select2-results__option[role=group] .select2-results__group {
            background-color: #333333;
            color: #fff;
        }

        .select2-container .select2-search--inline .select2-search__field {
            margin-top: 0 !important;
        }

        .select2-container .select2-search--inline .select2-search__field:focus {
            border-bottom: none !important;
            box-shadow: none !important;
        }

        .select2-container .select2-selection--multiple {
            min-height: 2.05rem !important;
        }

        .select2-container--default.select2-container--disabled .select2-selection--single {
            background-color: #ddd !important;
            color: rgba(0,0,0,0.26);
            border-bottom: 1px dotted rgba(0,0,0,0.26);
        }
    </style>
</head>
<body>
    <!-- import JQuery -->
    <script type="text/javascript" src="<?=asset('jquery-3.6.0.min.js')?>"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- import materialize.min.js -->
    <script type="text/javascript" src="<?=asset('materialize/js/materialize.min.js')?>"></script>

    <?php
        /**
         * Page Setup
         */
        $layout = 'dashboard';
        $page = 'home';
        if(isset($_GET['page']))
        {
            $page = $_GET['page'];
        }

        // Calling Page
        include setup_page($layout, $page);
    ?>

    <script>
    // Alert
        <?php
            echo ($session->issetSession('error')) ? "// Error (Ada)" : "// Error (Tidak)";
            if($session->issetSession('error'))
            {
        ?>

            text = "<i class='material-icons'>error_outline</i> &nbsp;&nbsp;<?=$session->getSession('error')?>"
            M.toast({html: text, classes: 'red lighten-2'})
        <?php
                $session->unsetSession('error');
            }
        ?>

        <?php
            echo ($session->issetSession('warning')) ? "// Warning (Ada)" : "// Warning (Tidak)";
            if($session->issetSession('warning'))
            {
        ?>

            text = "<i class='material-icons'>warning</i> &nbsp;&nbsp;<?=$session->getSession('warning')?>"
            M.toast({html: text, classes: 'yellow lighten-2'})
        <?php
                $session->unsetSession('warning');
            }
        ?>

        <?php
            echo ($session->issetSession('success')) ? "// Success (Ada)" : "// Success (Tidak)";
            if($session->issetSession('success'))
            {
        ?>

            text = "<i class='material-icons'>check_circle</i> &nbsp;&nbsp;<?=$session->getSession('success')?>"
            M.toast({html: text, classes: 'green lighten-2'})
        <?php
                $session->unsetSession('success');
            }
        ?>
        
    // End Alert
    
    // Custom JS
        $(document).ready(function(){
            // Dropdown Components
            $(".dropdown-trigger").dropdown({
                coverTrigger: false,
                constrainWidth: false,
            });

            // Select Input
            $('select:not(.select2)').formSelect();
            $('.select2').select2({width: "100%"});

            // Floating Button
            $('.fixed-action-btn').floatingActionButton();

            // Tooltip
            $('.tooltipped').tooltip();

            // Modal
            $('.modal').modal();
        })
    </script>
</body>
</html>