<?php
require_once 'Model.php';

class DataTraining extends Model
{
    public $name = 'Data Training';
    public $table = 'data_trainings';
    public $primaryKey = 'data_training_id';
    protected $columns = ['name', 'penghasilan_ayah', 'penghasilan_ibu', 'penerima_kps', 'penerima_kip', 'jenis_transportasi', 'result'];

    public function getValueExport($column, $value)
    {
        $penghasilan_ayah = [
            "0" => "Tidak Berpenghasilan",
            "1" => "< 500.000",
            "2" => "500.000 - 1.000.000",
            "3" => "1.000.000 - 2.000.000",
            "4" => "> 2.000.000",
        ];
    
        $penghasilan_ibu = [
            "0" => "Tidak Berpenghasilan",
            "1" => "< 500.000",
            "2" => "500.000 - 1.000.000",
            "3" => "1.000.000 - 2.000.000",
            "4" => "> 2.000.000",
        ];
    
        $jenis_transportasi = [
            "1" => "Jalan Kaki",
            "2" => "Sepeda",
            "3" => "Sepeda Motor",
            "4" => "Mobil",
        ];

        if($column == 'penghasilan_ayah')
        {
            $value = $penghasilan_ayah[$value];
        }

        if($column == 'penghasilan_ibu')
        {
            $value = $penghasilan_ibu[$value];
        }

        if($column == 'jenis_transportasi')
        {
            $value = $jenis_transportasi[$value];
        }
        
        if($column == 'penerima_kps')
        {
            if($value == 1)
            {
                $value = 'YA';
            }else{
                $value = 'TIDAK';
            }
        }

        if($column == 'penerima_kip')
        {
            if($value == 1)
            {
                $value = 'YA';
            }else{
                $value = 'TIDAK';
            }
        }

        return $value;
    }

    public function getValueImport($column, $value)
    {
        $penghasilan_ayah = [
            "Tidak Berpenghasilan" => "0",
            "< 500.000" => "1",
            "500.000 - 1.000.000" => "2",
            "1.000.000 - 2.000.000" => "3",
            "> 2.000.000" => "4",
        ];
    
        $penghasilan_ibu = [
            "Tidak Berpenghasilan" => "0",
            "< 500.000" => "1",
            "500.000 - 1.000.000" => "2",
            "1.000.000 - 2.000.000" => "3",
            "> 2.000.000" => "4",
        ];
    
        $jenis_transportasi = [
            "Jalan Kaki" => "1",
            "Sepeda" => "2",
            "Sepeda Motor" => "3",
            "Mobil" => "4",
        ];

        if($column == 'penghasilan_ayah')
        {
            $value = $penghasilan_ayah[$value];
        }

        if($column == 'penghasilan_ibu')
        {
            $value = $penghasilan_ibu[$value];
        }

        if($column == 'jenis_transportasi')
        {
            $value = $jenis_transportasi[$value];
        }
        
        if($column == 'penerima_kps')
        {
            if(strtoupper($value) == 'YA')
            {
                $value = 1;
            }else{
                $value = 0;
            }
        }

        if($column == 'penerima_kip')
        {
            if(strtoupper($value) == 'YA')
            {
                $value = 1;
            }else{
                $value = 0;
            }
        }

        return $value;
    }
}
?>