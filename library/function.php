<?php
    /**
     * Functions
     */

    function url($path = '/', $id = null)
    {
        return URL.$path.(($id != null) ? '/'.$id : '');
    }

    function asset($path)
    {
        return url('//assets/'.$path);
    }

    function public_url($path)
    {
        return url('//public/'.$path);
    }

    function public_path($path)
    {
        return ROOT.'/public//'.$path;
    }

    function load_controller($controller)
    {
        return ROOT.'/controller//'.$controller.'.php';
    }

    function load_page($page)
    {
        $page = str_replace('.', '/', $page);
        return ROOT.'/page//'.$page.'.php';
    }

    function load_layout($layout)
    {
        return ROOT.'/page/layout//'.$layout.'.php';
    }

    function load_component($component)
    {
        return ROOT.'/page/components//'.$component.'.php';
    }

    function setup_page($layout, $page)
    {
        $page = load_page($page);

        return load_layout($layout);
    }
    
    function getRequest()
    {
        $request = [];
        foreach ($_POST as $key => $value) {
            if(is_array($value)){
                foreach ($value as $key_child => $val) {
                    $request[htmlspecialchars($key)][$key_child] = htmlspecialchars($val);
                }
            }else{
                $request[htmlspecialchars($key)] = htmlspecialchars($value);
            }
        }
        return $request;
    }
    
    function arrayUpdate($array, $key, $value)
    {
        unset($array[$key]);
        $array[htmlspecialchars($key)] = htmlspecialchars($value);

        return $array;
    }

    function contains($needle, $haystack)
    {
        return strpos($haystack, $needle) !== false;
    }

    function array_avg($array)
    {
        if(count($array) > 0)
        {
            $array_filtered = array_filter($array, function($v){
                return is_numeric($v);
            });
            return array_sum($array_filtered)/count($array_filtered);
        }else{
            return 0;
        }
    }

    function fileUpload($file)
    {
        $filename = $file['name'];
        $tmp_path = $file['tmp_name'];

        $new_path = ROOT."//public/".$filename;
        $uploaded = move_uploaded_file($tmp_path, $new_path);

        if ($uploaded) {
            return pathinfo($new_path);
        } else {
            return false;
        }
    }

    function eucDistance(array $a, array $b) {
        return
        array_sum(
            array_map(
                function($x, $y) {
                    return abs($x - $y) ** 2;
                }, $a, $b
            )
        ) ** (1/2);
    }

    function debug_die($array)
    {
        echo json_encode($array);
        die();
    }
?>