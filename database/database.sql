  --
  -- Database: `knn_beasiswa_m`
  --

  DROP DATABASE IF EXISTS knn_beasiswa_m;

  CREATE DATABASE knn_beasiswa_m;
  USE knn_beasiswa_m;

  -- --------------------------------------------------------

  --
  -- Table structure for table `mahasiswas`
  --
  DROP TABLE IF EXISTS `mahasiswas`;
  CREATE TABLE `mahasiswas` (
    `mahasiswa_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `penghasilan_ayah` int(11) NOT NULL,
    `penghasilan_ibu` int(11) NOT NULL,
    `penerima_kps` tinyint(1) NOT NULL DEFAULT '0',
    `penerima_kip` tinyint(1) NOT NULL DEFAULT '0',
    `jenis_transportasi` int(11) NOT NULL,
    `k` int(11) NOT NULL DEFAULT '3',
    `result` enum('Layak','Tidak Layak') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `sudah_diuji` tinyint(1) NOT NULL DEFAULT '0',
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`mahasiswa_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `data_trainings`
  --
  DROP TABLE IF EXISTS `data_trainings`;
  CREATE TABLE `data_trainings` (
    `data_training_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `penghasilan_ayah` int(11) NOT NULL,
    `penghasilan_ibu` int(11) NOT NULL,
    `penerima_kps` tinyint(1) NOT NULL DEFAULT '0',
    `penerima_kip` tinyint(1) NOT NULL DEFAULT '0',
    `jenis_transportasi` int(11) NOT NULL,
    `result` enum('Layak','Tidak Layak') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`data_training_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;