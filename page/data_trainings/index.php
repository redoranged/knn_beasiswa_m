<?php
    $model_data_training = new DataTraining();
    $data_trainings = $model_data_training->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Training',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/data_trainings/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-data_trainings'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/data_trainings/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';

    $modal = [
        'id' => 'modal-data_trainings',
        'model' => 'data_trainings',
        'text' => 'Excel Template : <a href="'.public_url('template/training-template.xlsx').'" download/>training-template.xlsx</a>',
        'action' => url('/data_trainings/import'),
    ];

    include_once load_component('modal-import');
?>    