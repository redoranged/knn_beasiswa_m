<?php
    $penghasilan_ayah = [
        "0" => "Tidak Berpenghasilan",
        "1" => "< 500.000",
        "2" => "500.000 - 1.000.000",
        "3" => "1.000.000 - 2.000.000",
        "4" => "> 2.000.000",
    ];

    $penghasilan_ibu = [
        "0" => "Tidak Berpenghasilan",
        "1" => "< 500.000",
        "2" => "500.000 - 1.000.000",
        "3" => "1.000.000 - 2.000.000",
        "4" => "> 2.000.000",
    ];

    $jenis_transportasi = [
        "1" => "Jalan Kaki",
        "2" => "Sepeda",
        "3" => "Sepeda Motor",
        "4" => "Mobil",
    ];

    $k = [1=>1,3=>3,5=>5];
?>
<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Data Mahasiswa</span>
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Penghasilan Ayah</th>
                    <th>Penghasilan Ibu</th>
                    <th width="80px">KPS</th>
                    <th width="80px">KIP</th>
                    <th>Jenis Transportasi</th>
                    <th>Hasil</th>
                    <th width="125px">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($mahasiswas as $key => $row) 
            {
            ?>
                <tr>
                    <td><?= $row['name'] ?></td>
                    <td><?= $penghasilan_ayah[$row['penghasilan_ayah']] ?></td>
                    <td><?= $penghasilan_ibu[$row['penghasilan_ibu']] ?></td>
                    <td><?= ($row['penerima_kps']) ? '<i class="material-icons green-text">check</i>' : '<i class="material-icons red-text">clear</i>' ?></td>
                    <td><?= ($row['penerima_kip']) ? '<i class="material-icons green-text">check</i>' : '<i class="material-icons red-text">clear</i>' ?></td>
                    <td><?= $jenis_transportasi[$row['jenis_transportasi']] ?></td>
                    <td><?= ($row['result'] == 'Layak') ? '<span class="green-text">'.$row['result'].'</span>' : '<span class="red-text">'.$row['result'].'</span>' ?></td>
                    <td>
                        <form action="<?=url('/mahasiswas/destroy', $row['mahasiswa_id'])?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row['name'] ?>?');" method="post">
                            <input type="hidden" name="model" value=mahasiswas id="model"/>
                            <a href="<?=url('/mahasiswas/edit', $row['mahasiswa_id'])?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <input name="mahasiswa_id" id="mahasiswa_id" value="<?= $row['mahasiswa_id'] ?? ''?>" type="hidden"/>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        </form>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>