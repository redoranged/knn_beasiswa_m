<?php
    $model_mahasiswa = new Mahasiswa();
    $mahasiswas = $model_mahasiswa->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Mahasiswa',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/mahasiswas/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-mahasiswas'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/mahasiswas/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';

    $modal = [
        'id' => 'modal-mahasiswas',
        'model' => 'mahasiswas',
        'text' => 'Excel Template : <a href="'.public_url('template/mahasiswa-template.xlsx').'" download/>mahasiswa-template.xlsx</a>',
        'action' => url('/mahasiswas/import'),
    ];

    include_once load_component('modal-import');
?>    