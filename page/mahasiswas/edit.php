<?php
    $model_mahasiswa = new Mahasiswa();
    $mahasiswa = $model_mahasiswa->find($_GET['id']);

    
    $model_data_training = new DataTraining();
    $data_trainings = $model_data_training->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Mahasiswa',
            'link' => url('/mahasiswas')
        ],
        [
            'title' => $mahasiswa['name'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');

    if(count($data_trainings) > 0)
    {
        // Floating Button Setup
        $button_items = [
            [
                'name' => 'Proses Data',
                'icon' => 'cached',
                'class' => 'orange',
                'link' => url('/mahasiswas/proses_data', $mahasiswa['mahasiswa_id'])
            ]
        ];
        include_once load_component('floating-button');
    }
?>
<br>
<?php
    include 'view/form.php';
    
?>